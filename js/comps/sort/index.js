(function($) {
    'use strict';

    /**
     * Sort component.
     * @namespace Sort
     * @memberOf PF.comp
     */

    window.PF = window.PF || {};
    window.PF.comp = window.PF.comp || {};

    PF.comp.Sort = (function() {

        /**
         * Sort component.
         *
         * @memberOf PF.comp.Sort
         * @constructor
         * @param {DOM} elem Markup for the component.
         */
        function Comp(elem) {
            this.elem = elem;
            this.$elem = $(elem);
            this.subscribes = [];
            this.$from = this.$elem.find('.sort__from');
            this.$to = this.$elem.find('.sort__to');
            this.$error = this.$elem.find('.sort__error');
            this.sortKind = this.$elem.find('[type="radio"]:checked').val();
        }

        Comp.prototype = {
            /**
             * Initialize the component.
             *
             * @memberOf PF.comp.Sort
             */
            init: function() {
                this.unbind();
                this.$elem.bind('submit.Sort', this.onSubmit.bind(this));
                this.$elem.find('[type="radio"]').bind('click.Sort', this.onClick.bind(this));
                this.subscribes.push($.subscribe('list:reset', this.showComponent.bind(this)));
            },

            /**
             * Reinitialize the component.
             *
             * @memberOf PF.comp.Sort
             */
            reinit: function() {
                this.init();
            },

            /**
             * Unbind all subscriptions and binds for the component.
             *
             * @memberOf PF.comp.Sort
             */
            unbind: function() {
                this.subscribes.forEach(function(elem) {
                    $.unsubscribe(elem);
                });
                this.$elem.unbind('click.Sort');
            },

            /**
             * Destroys the component.
             *
             * @memberOf PF.comp.Sort
             */
            destroy: function() {
                this.unbind();
                $(this.elem).removeData('component');
            },

            /**
             * Submit handler for from
             *
             * @memberOf PF.comp.Sort
             */
            onSubmit: function() {
                var isValid = this.$from.val().length && this.$to.val().length;

                if (isValid) {
                    this.fetch();
                }

                this.$error.toggleClass('sort__error--visible', !isValid);

                return false;
            },

            /**
             * Click handler for sort option
             *
             * @memberOf PF.comp.Sort
             */
            onClick: function(e) {
                this.sortKind = e.currentTarget.value;
            },

            /**
             * Filter data from user selection.
             *
             * @memberOf PF.comp.Sort
             * @param {object} item Data of the item
             */
            filterResults: function(item) {
                return item.departure === this.$from.val() &&
                    item.arrival === this.$to.val();
            },

            /**
             * Sort data from user selection
             *
             * @memberOf PF.comp.Sort
             * @param {object} first Data of the first item
             * @param {object} second Data of the second item
             */
            sortResults: function(first, second) {
                if (this.sortKind === 'duration') {
                    var firstTotal = (first.duration.h * 60) + parseInt(first.duration.m, 10);
                    var secondTotal = (second.duration.h * 60) + parseInt(second.duration.m, 10);

                    return firstTotal - secondTotal;
                } else {
                    return first.cost - second.cost;
                }
            },

            /**
             * Shows the component
             *
             * @memberOf PF.comp.Sort
             */
            showComponent: function() {
                this.$elem.removeClass('sort--hidden');
            },

            /**
             * Hides the component
             *
             * @memberOf PF.comp.Sort
             */
            hideComponent: function() {
                this.$elem.addClass('sort--hidden');
            },

            /**
             * Fetch data from an api, filters and sorts result
             *
             * @memberOf PF.comp.Sort
             */
            fetch: function() {
                $.get('response.json', {
                    from: this.$from.val(),
                    to: this.$to.val()
                }, function(data) {

                    data.deals = data.deals.filter(this.filterResults.bind(this));

                    data.deals = data.deals.sort(this.sortResults.bind(this));

                    this.hideComponent();

                    $.publish('sort:fetched', [data]);
                }.bind(this));
            }

        };

        return Comp;
    })();

}(jQuery));
