(function($) {
    'use strict';

    /**
     * List component.
     * @namespace List
     * @memberOf PF.comp
     */

    window.PF = window.PF || {};
    window.PF.comp = window.PF.comp || {};

    PF.comp.List = (function() {

        /**
         * List component.
         *
         * @memberOf PF.comp.List
         * @constructor
         * @param {DOM} elem Markup for the component.
         */
        function Comp(elem) {
            this.elem = elem;
            this.$elem = $(elem);
            this.$list = this.$elem.find('ul');
            this.subscribes = [];
        }

        Comp.prototype = {
            /**
             * Initialize the component.
             *
             * @memberOf PF.comp.List
             */
            init: function() {
                this.unbind();
                this.subscribes.push($.subscribe('sort:fetched', this.updateList.bind(this)));
                this.$elem.find('[type="button"]').bind('click.Sort', this.onClick.bind(this));
            },

            /**
             * Reinitialize the component.
             *
             * @memberOf PF.comp.List
             */
            reinit: function() {
                this.init();
            },

            /**
             * Unbind all subscriptions and binds for the component.
             *
             * @memberOf PF.comp.List
             */
            unbind: function() {
                this.subscribes.forEach(function(elem) {
                    $.unsubscribe(elem);
                });
                this.$elem.unbind('click.List');
            },

            /**
             * Destroys the component.
             *
             * @memberOf PF.comp.List
             */
            destroy: function() {
                this.unbind();
                $(this.elem).removeData('component');
            },

            /**
             * onClick handler for reset button.
             * It clears list and hides component.
             *
             * @memberOf PF.comp.List
             */
            onClick: function() {
                this.$list.html('');
                this.$elem.removeClass('list--visible');
                $.publish('list:reset');
            },

            /**
             * Builds string of item and appends to list of a given item
             *
             * @memberOf PF.comp.List
             * @param {string} currency Currency of price
             * @param {object} item Data of the item
             */
            renderItem: function(currency, item) {
                this.$list.append('<li class="list__item">' + item.arrival + ' &gt; ' + item.departure +
                    ' ' + item.cost + ' ' + currency + '<br>' +
                    item.transport + ' ' + item.reference + ' for ' +
                    item.duration.h + 'h' + item.duration.m +
                    '</li>');
            },

            /**
             * Updates DOM from a given list
             *
             * @memberOf PF.comp.List
             * @param {array} list Given list to update
             */
            updateList: function(list) {
                this.$list.html('');

                this.$elem.toggleClass('list--visible', list.deals.length);

                list.deals.map(this.renderItem.bind(this, list.currency));
            }

        };

        return Comp;
    })();

}(jQuery));
