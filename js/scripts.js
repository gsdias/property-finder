/**
 * @namespace jQuery
 */

/**
 * @namespace jQuery.fn
 */

/**
 * @namespace
 */
var PF = window.PF || {};

/**
 * @namespace
 */
PF.common = window.PF.common || {};

/**
 * @namespace
 */
PF.comp = window.PF.comp || {};

(function($) {
    'use strict';

    var pubsubCache = [];

    /**
     * Publish method. It will run all callback functions for given topic
     * @memberOf jQuery
     * @param {string} topic Topic of the publish action
     * @param {array} args Arguments to send to callback function
     */
    $.publish = function(topic, args) {
        pubsubCache[topic] && $.each(pubsubCache[topic], function() {
            this.apply($, args || []);
        });
    };

    /**
     * Subscribe method. Saves callback function for given topic
     * @memberOf jQuery
     * @param {string} topic Topic of the subscribe action
     * @param {function} callback Callback function
     */
    $.subscribe = function(topic, callback) {
        if (!pubsubCache[topic]) {
            pubsubCache[topic] = [];
        }
        pubsubCache[topic].push(callback);
        return [topic, callback];
    };

    /**
     * Unsubscribe method. Removes callback from saved list
     * @memberOf jQuery
     * @param {array} handle array containing topic and callback function to be removed
     */
    $.unsubscribe = function(handle) {
        var t = handle[0];
        pubsubCache[t] && $.each(pubsubCache[t], function(idx) {
            if (this === handle[1]) {
                pubsubCache[t].splice(idx, 1);
            }
        });
    };

    /**
     * Checks if element exists in the DOM and executes given callback
     * @memberOf jQuery.fn
     * @param {function} func Callback to be executed
     */
    $.fn.doIfExists = function(func) {
        if (this.length) {
            func.apply(this, arguments);
        }
        return this;
    };

    /**
     * Checks if element does not exist in the DOM and executes given callback
     * @memberOf jQuery.fn
     * @param {function} func Callback to be executed
     */
    $.fn.doIfNotExists = function(func) {
        if (!this.length) {
            func.apply(this, arguments);
        }
        return this;
    };

    /**
     * Given a component it will check if exists in DOM and initialize it
     * @memberOf PF.comp
     * @param {string} name Name of the component
     * @param {DOM} $item Markup of the component
     */
    PF.comp.init = function(name, $item) {
        $item.doIfExists(function() {
            if (typeof this.data('component') === 'undefined') {
                this.data('component', new PF.comp[name](this[0]));
                this.data('component').init();
            } else {
                this.data('component').reinit();
            }
        });
    };

    /**
     * Loop through a list of components and call init for component
     * @memberOf PF.comp
     * @param {array} $components List of components to initialize
     * @param {bool} checkDefer Flag to check if we skip defer option
     */
    PF.comp.loop = function($components, checkDefer) {

        var total = 0;
        $.each($components, function(index, item) {
            var $item = $(item);

            if (!checkDefer || !$item.closest('[data-comp-defer="true"]').length) {
                var name = $item.data('comp');
                PF.comp.init(name, $item);
                total++;
            }
        });
    };

    /**
     * Initialize common functions
     * @memberOf PF.common
     */
    PF.common.initialize = function($scope) {
        $scope = $scope || $('body');
        var $components = $scope.find('[data-comp]');
        PF.comp.loop($components, 1);
    };

    /**
     * Destroy components
     * @memberOf PF.common
     */
    PF.common.destroy = function($scope) {
        $scope = $scope || $('body');
        var $components = $scope.find('[data-comp]');
        $.each($components, function(index, item) {
            $(item).doIfExists(function() {
                if (typeof this.data('component') !== 'undefined') {
                    this.data('component').destroy();
                }
            });
        });
    };

    $(document).ready(PF.common.initialize);

}(jQuery));
