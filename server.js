const http = require('http');
const express = require('express');
const app = express();

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
});

const server = http.createServer(app);

require('./setup')(app, server);

app.listen(3000, () => {
    console.log('port:', 3000);
});
