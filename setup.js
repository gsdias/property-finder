const serveStatic = require('serve-static');

module.exports = (app, server) => {
    'use strict';

    app.use('/', serveStatic('dist'));
    app.use('/server', serveStatic('server'));

    /// Connect Error Handler
    /// Simple stubbed error handler for the time being
    /// This should be fleshed out more.s
    app.use(function onerror(err, req, res, next) {
        if (res.headersSent) {
            return next(err);
        }
        res.status(err.status || 500);


    });
};
