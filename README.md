# README #

This solution could be built in React using Redux as a way to control the state of all components in a shared store as well.

I used that approach as well in React Native where store from Redux retains the state of components.

You can install locally `npm i` and run `npm start` or you can just open dist folder but api call wont work because 'Cross origin'