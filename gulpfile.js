'use strict';

const gulp = require('gulp');
const clean = require('gulp-clean');

const jscs = require('gulp-jscs');
const jshint = require('gulp-jshint');

const concat = require('gulp-concat');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const postcss = require('gulp-postcss');

const sassConfig = {
    outputStyle: 'expanded',
    errLogToConsole: true
};

const autoprefixerOptions = {
    browsers: [
        'last 2 versions',
        'ie >= 9',
        'Firefox >= 40',
        'Chrome >= 45',
        'Safari >= 7'
    ]
};

gulp.task('sass', () => {
    var postcsstasks = [require('autoprefixer')(autoprefixerOptions)];

    gulp.src(['scss/*'])
        .pipe(sass(sassConfig).on('error', sass.logError))
        .pipe(postcss(postcsstasks))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('jscs', () => {
    gulp.src('components/**/js/*.js')
        .pipe(jscs())
        .pipe(jscs.reporter());
});

gulp.task('concat', () => {
    gulp.src('js/**/*.js')
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('views', () => {
    gulp.src(['views/**'])
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', () => {
    gulp.watch(['scss/*.scss', 'scss/**/*.scss'], ['sass']);
    gulp.watch('js/**/*.js', ['concat']);
    gulp.watch('views/*.js', ['views']);
});

gulp.task('cleanfolder', () => {
    gulp.src(['dist/*'], { read: false })
        .pipe(clean());
    gulp.src(['views/**', 'server/*'])
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['cleanfolder', 'watch', 'views', 'concat', 'sass']);
